<h1 align="center">Welcome to ina-ww2-grabber 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/curl--blue.svg" />
  <img src="https://img.shields.io/badge/youtube-dl--blue.svg" />
</p>

> A small bash script that download every WW2 archives hosted on the INA's website

## Prerequisites

- curl 
- youtube-dl 

## Install

```sh
Just install the dependencies, then download and run the script.
```

## Usage

```sh
./ina_grab_vichy.bash
```

## Author

👤 **NotEvenOnce**


## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_