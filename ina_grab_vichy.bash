#!/bin/bash
# Télécharge les vidéos disponibles sur l'INA et datant entre 1940 et 1945
# Environ 28Go de vidéo d'archives vont être téléchargés 
# Nécessite les paquets youtube-dl et curl pour fonctionner
url="https://www.ina.fr/video/AFE"
get_http_code(){
        readarray -t http_get < <( curl --write-out '%{http_code}' --silent -S "${1}" || return 1 ) # télécharge la page et son code http
        http_code=$( printf '%s\n' "${http_get[@]}" | tail -n 1 )
        video_date=$( printf '%s\n' "${http_get[@]}" | grep "broadcast" | grep -o "194[0-5]" ) # contien la date du document seulement si celle-ci est entre 1940 et 1945
        echo "${i} - ${http_code} - ${video_date}"
}
get_video(){
        # video_name=$( printf '%s\n' "${http_get[@]}" | grep '"h2--title"' | sed 's/<[^>]*>//g;s/^[ \t]*//' ) # pas utilisé actuellement
        video_author=$( printf '%s\n' "${http_get[@]}" | grep -A1 '"h3--title"' | sed 's/<[^>]*>//g;s/^[ \t]*//;/^$/d' )
        mkdir -p "${HOME}"/INA/"${video_author}" # crée le répertoire lié à l'auteur
        youtube-dl -o "$HOME/INA/${video_author}/${video_date}-%(title)s-%(id)s.%(ext)s" "$1" # télécharge la vidéo dans le répertoire assigné
}
loop_function(){
        get_http_code ${url}"${i}" || return 1 
        if [[ ${http_code} -eq 200 ]] && [[ -n ${video_date} ]]; then
                ((find++))
                while [[ $(ps u -C youtube-dl | wc -l) -gt 6 ]]; do
                        sleep $((RANDOM % 5))
                done # pas plus de cinq téléchargements simultanés pour éviter de crasher le site web 
                get_video ${url}"${i}"
        fi
}
for i in {85000000..85004999} {86000000..86004999} {09000000..09004999}; do
        sleep 0.25 # lance en tâche de fond une nouvelle requête tout les 25ms
        (loop_function || sleep $((RANDOM % 10)) && loop_function) &
done
echo "${find} vidéos ont été trouvés."